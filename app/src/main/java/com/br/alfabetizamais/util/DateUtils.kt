package com.br.alfabetizamais.util

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

const val PATTERN_SLASH_DATE = "dd/MM/yyyy"

fun locale() = Locale("pt", "BR")

@RequiresApi(Build.VERSION_CODES.O)
fun formatDate(unformattedDate: String): String {
    val localDate = LocalDate.parse(unformattedDate)
    return formatLocalDate(localDate)
}

@RequiresApi(Build.VERSION_CODES.O)
fun formatLocalDate(localDate: LocalDate): String {
    return localDate.format(DateTimeFormatter.ofPattern(PATTERN_SLASH_DATE).withLocale(locale()))
}
