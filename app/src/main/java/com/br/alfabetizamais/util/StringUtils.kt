package com.br.alfabetizamais.util

import java.text.Normalizer
import java.util.*

fun CharSequence.unaccent(): String {
    return Normalizer
        .normalize(this, Normalizer.Form.NFD)
        .replace("[^\\p{ASCII}]".toRegex(), "").lowercase(Locale.getDefault())
}