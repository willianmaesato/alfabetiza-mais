package com.br.alfabetizamais.navigation

sealed class Screen(val route: String, val title: String){
    object HomeScreen: Screen("HomeScreen", "Home")
    object GradeAverageScreen: Screen("GradeAverageScreen", "Notas")
    object TestExamsToDoScreen: Screen("TestExamsToDoScreen", "Provas")
    object LoginScreen: Screen("LoginScreen", "Login")
    object SplashScreen: Screen("SplashScreen", "Splash")
    object EnterScreen: Screen("EnterScreen", "Enter")
}