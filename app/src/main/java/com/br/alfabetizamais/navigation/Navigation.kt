package com.br.alfabetizamais.navigation

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.br.alfabetizamais.domain.model.TestExamData
import com.br.alfabetizamais.features.enter.presenter.EnterScreen
import com.br.alfabetizamais.features.enter.presenter.login.LoginScreen
import com.br.alfabetizamais.features.home.HomeScreen
import com.br.alfabetizamais.features.profile.GradleTestExamsScreen
import com.br.alfabetizamais.features.splash.SplashScreen
import com.br.alfabetizamais.features.test_exams.to_do.TestExamsToDoScreen

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun Navigation(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = Screen.SplashScreen.route,
        modifier = Modifier.fillMaxSize()
    ) {
        composable(Screen.SplashScreen.route) {
            SplashScreen(navController)
        }
        composable(Screen.EnterScreen.route) {
            EnterScreen(navController)
        }
        composable(Screen.LoginScreen.route) {
            LoginScreen(navController)
        }
        composable(Screen.HomeScreen.route) {
            HomeScreen(navController)
        }
        composable(Screen.GradeAverageScreen.route) {
            GradleTestExamsScreen(navController)
        }
        composable(Screen.TestExamsToDoScreen.route) {
            val testExamItem =
                navController.previousBackStackEntry?.savedStateHandle?.get<TestExamData>("testExamItem")
            val idAluno =
                navController.previousBackStackEntry?.savedStateHandle?.get<Int>("idAluno")
            testExamItem?.let { testExam ->
                TestExamsToDoScreen(
                    navController, testExam = testExam, idAluno = idAluno!!
//                testExamsData = TestExamsData(
//                    prova = listOf(
//                        ProvaData(
//                            id = 1,
//                            idProva = 2,
//                            idExercicio = 1,
//                            valorExercicio = 1,
//                            ordem = 1,
//                            exercicio = ExercicioData(
//                                id = 1,
//                                name = "nome 1",
//                                enunciado = "enunciado 1",
//                                imagemEnunciado = "imagem",
//                                textoAuxiliar = "texto_auxiliar 1",
//                                materia = "materia",
//                                tipoExercicio = 1,
//                                enunciadoQuestaoA = "Enunciado da respsota A",
//                                enunciadoQuestaoB = "Enunciado da respsota B",
//                                enunciadoQuestaoC = "Enunciado da resposta D",
//                                enunciadoQuestaoD = "Enunciado da respsota D",
//                                enunciadoQuestaoE = "Enunciado da respsota E",
//                                respostaQuestao = "B",
//                            )
//                        ),
//                        ProvaData(
//                            id = 2,
//                            idProva = 2,
//                            idExercicio = 2,
//                            valorExercicio = 1,
//                            ordem = 2,
//                            exercicio = ExercicioData(
//                                id = 2,
//                                name = "nome 2",
//                                enunciado = "enunciado 2",
//                                imagemEnunciado = "imagem",
//                                textoAuxiliar = "texto_auxiliar 2",
//                                materia = "materia",
//                                tipoExercicio = 2,
//                                enunciadoQuestaoA = "Enunciado da respsota A",
//                                enunciadoQuestaoB = "Enunciado da respsota B",
//                                enunciadoQuestaoC = "Enunciado da respsota c",
//                                enunciadoQuestaoD = "Enunciado da respsota D",
//                                enunciadoQuestaoE = "Enunciado da respsota E",
//                                respostaQuestao = "B@C"
//                            )
//                        ),
//                        ProvaData(
//                            id = 3,
//                            idProva = 3,
//                            idExercicio = 3,
//                            valorExercicio = 1,
//                            ordem = 3,
//                            exercicio = ExercicioData(
//                                id = 3,
//                                name = "nome 3",
//                                enunciado = "enunciado 3",
//                                imagemEnunciado = "imagem",
//                                textoAuxiliar = "texto_auxiliar 3",
//                                materia = "materia",
//                                tipoExercicio = 3,
//                            )
//                        ),
//                        ProvaData(
//                            id = 3,
//                            idProva = 3,
//                            idExercicio = 3,
//                            valorExercicio = 1,
//                            ordem = 3,
//                            exercicio = ExercicioData(
//                                id = 4,
//                                name = "nome 4",
//                                enunciado = "enunciado 4",
//                                imagemEnunciado = "imagem",
//                                textoAuxiliar = "texto_auxiliar 4",
//                                materia = "materia",
//                                tipoExercicio = 4,
//                                numeroA = 10,
//                                numeroB = 20,
//                                operacao = "+",
//                                respostaQuestao = "30"
//                            ),
//                        ),
//                        ProvaData(
//                            id = 3,
//                            idProva = 3,
//                            idExercicio = 3,
//                            valorExercicio = 1,
//                            ordem = 3,
//                            exercicio = ExercicioData(
//                                id = 5,
//                                name = "nome 5",
//                                enunciado = "enunciado 5",
//                                imagemEnunciado = "imagem",
//                                textoAuxiliar = "texto_auxiliar 5",
//                                materia = "materia",
//                                tipoExercicio = 5,
//                                numeroMinimo = 10,
//                                numeroMaximo = 20,
//                                respostaQuestao = "10@15@17"
//                            ),
//                        ),
//                        ProvaData(
//                            id = 3,
//                            idProva = 3,
//                            idExercicio = 3,
//                            valorExercicio = 1,
//                            ordem = 3,
//                            exercicio = ExercicioData(
//                                id = 6,
//                                name = "nome 6",
//                                enunciado = "enunciado 6",
//                                imagemEnunciado = "imagem",
//                                textoAuxiliar = "texto_auxiliar 6",
//                                materia = "materia",
//                                tipoExercicio = 6,
//                                exercicioTipoSeis = listOf(
//                                    QuestaoPathData(
//                                        imagem = "path",
//                                        idExercicio = 1,
//                                        resposta = "A",
//                                        id = 1
//                                    ),
//                                    QuestaoPathData(
//                                        imagem = "path",
//                                        idExercicio = 2,
//                                        resposta = "A",
//                                        id = 2
//                                    ), QuestaoPathData(
//                                        imagem = "path",
//                                        idExercicio = 3,
//                                        resposta = "A",
//                                        id = 3
//                                    ),
//                                    QuestaoPathData(
//                                        imagem = "path",
//                                        idExercicio = 4,
//                                        resposta = "D",
//                                        id = 4
//                                    )
//                                )
//                            ),
//                        ),
//                    ),
//                )
                )
            }
        }
    }
}