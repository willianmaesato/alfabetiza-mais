package com.br.alfabetizamais.di

import com.br.alfabetizamais.data.PreferencesManager
import com.br.alfabetizamais.features.enter.data.repository.LoginRepositoryImpl
import com.br.alfabetizamais.features.enter.data.service.LoginService
import com.br.alfabetizamais.features.enter.presenter.login.LoginViewModel
import com.br.alfabetizamais.features.enter.use_case.LoginUseCase
import com.br.alfabetizamais.features.enter.use_case.ValidatePassword
import com.br.alfabetizamais.features.enter.use_case.ValidateUser
import com.br.alfabetizamais.features.home.HomeViewModel
import com.br.alfabetizamais.features.home.data.repository.AvailableTestExamsRepositoryImpl
import com.br.alfabetizamais.features.home.data.service.AvailableTestExamsService
import com.br.alfabetizamais.features.home.use_case.AvailableTestExamsUseCase
import com.br.alfabetizamais.features.profile.GradeAverageViewModel
import com.br.alfabetizamais.features.profile.data.repository.GradeAverageRepositoryImpl
import com.br.alfabetizamais.features.profile.data.service.GradeAverageService
import com.br.alfabetizamais.features.profile.use_case.GradeAverageUseCase
import com.br.alfabetizamais.features.test_exams.data.repository.TestExamsToDoToDoRepositoryImpl
import com.br.alfabetizamais.features.test_exams.data.service.TestExamsToDoService
import com.br.alfabetizamais.features.test_exams.to_do.TestExamsToDoViewModel
import com.br.alfabetizamais.features.test_exams.use_case.TestExamsToDoUseCase
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@ExperimentalSerializationApi
@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        val contentType = MediaType.get("application/json")
        val json = Json {
            ignoreUnknownKeys = true
        }

        return Retrofit.Builder()
            .baseUrl("https://salty-island-76150.herokuapp.com/api/v1/")
            .client(okHttpClient)
            .addConverterFactory(json.asConverterFactory(contentType))
            .build()
    }

    @Provides
    @Singleton
    fun provideLoginService(retrofit: Retrofit): LoginService {
        return retrofit.create(LoginService::class.java)
    }

    @Provides
    @Singleton
    fun provideTestExamsToDoService(retrofit: Retrofit): TestExamsToDoService {
        return retrofit.create(TestExamsToDoService::class.java)
    }

    @Provides
    @Singleton
    fun provideAvailableTestExamsToDoService(retrofit: Retrofit): AvailableTestExamsService {
        return retrofit.create(AvailableTestExamsService::class.java)
    }

    @Provides
    @Singleton
    fun provideGradeAverageService(retrofit: Retrofit): GradeAverageService {
        return retrofit.create(GradeAverageService::class.java)
    }

    @Provides
    fun providerLoginViewModel(
        preferencesManager: PreferencesManager,
        retrofit: Retrofit
    ): LoginViewModel {
        return LoginViewModel(
            validateUser = ValidateUser(),
            validatePassword = ValidatePassword(),
            preferencesManager = preferencesManager,
            loginUseCase = LoginUseCase(
                loginRepository = LoginRepositoryImpl(
                    service = provideLoginService(retrofit)
                )
            )
        )
    }

    @Provides
    fun providerHomeViewModel(
        preferencesManager: PreferencesManager,
        retrofit: Retrofit
    ): HomeViewModel {
        return HomeViewModel(
            preferencesManager = preferencesManager,
            availableTestExamsUseCase = AvailableTestExamsUseCase(
                availableTestExamsRepository = AvailableTestExamsRepositoryImpl(
                    availableTestExamsService = provideAvailableTestExamsToDoService(retrofit)
                )
            )
        )
    }

    @Provides
    fun providerTestExamsToDoViewModel(
        retrofit: Retrofit
    ): TestExamsToDoViewModel {
        return TestExamsToDoViewModel(
            testExamsToDoUseCase = TestExamsToDoUseCase(
                testExamsToDoRepository = TestExamsToDoToDoRepositoryImpl(
                    service = provideTestExamsToDoService(retrofit)
                )
            )
        )
    }

    @Provides
    fun providerGradeAverageViewModel(
        preferencesManager: PreferencesManager,
        retrofit: Retrofit
    ): GradeAverageViewModel {
        return GradeAverageViewModel(
            preferencesManager = preferencesManager,
            gradeAverageUseCase = GradeAverageUseCase(
                gradeAverageRepository = GradeAverageRepositoryImpl(
                    service = provideGradeAverageService(retrofit)
                )
            )
        )
    }
}