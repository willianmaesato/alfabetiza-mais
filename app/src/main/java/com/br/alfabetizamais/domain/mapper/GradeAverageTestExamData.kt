package com.br.alfabetizamais.domain.mapper

import kotlinx.serialization.Serializable

@Serializable
data class GradeAverageTestExamData(
    val idAluno: Int,
    val idProva: Int,
    val nota: Double,
    val exercisethree: Int
)
