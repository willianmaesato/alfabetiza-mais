package com.br.alfabetizamais.domain.model

import com.br.alfabetizamais.data.model.QuestaoPathResponse
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TestExamsData(
    val prova: ProvaData,
)

@Serializable
data class ExercicioData(
    val id: Int? = null,
    val name: String? = null,
    val materia: String? = null,

    @SerialName("tipo_exercicio")
    val tipoExercicio: Int? = null,

    @SerialName("texto_auxiliar")
    val textoAuxiliar: String? = null,

    @SerialName("imagem_enunciado")
    val imagemEnunciado: String? = null,

    val enunciado: String? = null,

    @SerialName("enunciado_questao_a")
    val enunciadoQuestaoA: String? = null,

    @SerialName("enunciado_questao_b")
    val enunciadoQuestaoB: String? = null,

    @SerialName("enunciado_questao_c")
    val enunciadoQuestaoC: String? = null,

    @SerialName("enunciado_questao_d")
    val enunciadoQuestaoD: String? = null,

    @SerialName("enunciado_questao_e")
    val enunciadoQuestaoE: String? = null,

    @SerialName("resposta_questao")
    val respostaQuestao: String? = null,

    @SerialName("numero_a")
    val numeroA: Int? = null,

    @SerialName("numero_b")
    val numeroB: Int? = null,

    val operacao: String? = null,

    @SerialName("numero_minimo")
    val numeroMinimo: Int? = null,

    @SerialName("numero_maximo")
    val numeroMaximo: Int? = null,

    val exercicioTipoSeis: Map<String, QuestaoPathData>? = null
)

@Serializable
data class QuestaoPathData(
    val id: Int,
    val idExercicio: Int,
    val imagem: String,
    val resposta: String
)

@Serializable
data class ProvaData(
    val id: Int,
    val idProva: Int,
    val idExercicio: Int,
    val ordem: Int,
    val valorExercicio: Int,
    val exercicio: ExercicioData
)