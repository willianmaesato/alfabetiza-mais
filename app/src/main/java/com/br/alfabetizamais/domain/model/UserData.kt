package com.br.alfabetizamais.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class UserData(
    val id: Int = -1,
    val idTurma: Int = -1,
    val nome: String = "",
    val usuario: String = "",
    val senha: String = "",
    val idade: Int = 0,
    val nomeCompletoResponsavel: String = "",
    val emailResponsavel: String = ""
)

