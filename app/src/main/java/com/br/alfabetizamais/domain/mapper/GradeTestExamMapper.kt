package com.br.alfabetizamais.domain.mapper

import com.br.alfabetizamais.data.model.GradeTestExamResponse
import com.br.alfabetizamais.domain.model.GradeTestExamData

fun GradeTestExamData.toRemote(): GradeTestExamResponse {
    return GradeTestExamResponse(
        idProva = idProva,
        idAluno = idAluno,
        nota = nota,
        exercisethree = exercisethree
    )
}