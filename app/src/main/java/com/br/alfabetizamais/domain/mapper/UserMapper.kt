package com.br.alfabetizamais.domain.mapper

import com.br.alfabetizamais.data.model.UserResponse
import com.br.alfabetizamais.domain.model.UserData

fun UserResponse.toDomain() = UserData(
    id = id,
    usuario = usuario,
    senha = senha,
    nomeCompletoResponsavel = nomeCompletoResponsavel,
    nome = nome,
    idTurma = idTurma,
    idade = idade,
    emailResponsavel = emailResponsavel
)

