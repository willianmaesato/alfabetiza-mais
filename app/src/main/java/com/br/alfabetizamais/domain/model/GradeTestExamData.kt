package com.br.alfabetizamais.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class GradeTestExamData(
    val idAluno: Int,
    val idProva: Int,
    val nota: Double,
    val exercisethree: Int,
    val mediaProva: Double,
    val valorProva: Int
)