package com.br.alfabetizamais.domain.mapper

import com.br.alfabetizamais.data.model.ExercicioResponse
import com.br.alfabetizamais.data.model.ProvaResponse
import com.br.alfabetizamais.data.model.QuestaoPathResponse
import com.br.alfabetizamais.data.model.TestExamsResponse
import com.br.alfabetizamais.domain.model.ExercicioData
import com.br.alfabetizamais.domain.model.ProvaData
import com.br.alfabetizamais.domain.model.QuestaoPathData
import com.br.alfabetizamais.domain.model.TestExamsData

fun TestExamsResponse.toDomain(): TestExamsData {
    return TestExamsData(
        prova = prova.toDomain()
    )
}

fun ProvaResponse.toDomain(): ProvaData {
    return ProvaData(
        id = id,
        idProva = idProva,
        ordem = ordem,
        valorExercicio = valorExercicio,
        idExercicio = idExercicio,
        exercicio = exercicio.toDomain()
    )
}

fun ExercicioResponse.toDomain(): ExercicioData {
    return ExercicioData(
        enunciado = enunciado ?: "",
        materia = materia ?: "",
        id = id ?: 0,
        textoAuxiliar = textoAuxiliar ?: "",
        tipoExercicio = tipoExercicio ?: 0,
        operacao = operacao ?: "",
        numeroA = numeroA ?: 0,
        numeroB = numeroB ?: 0,
        numeroMinimo = numeroMinimo ?: 0,
        numeroMaximo = numeroMaximo ?: 0,
        name = name ?: "",
        enunciadoQuestaoA = enunciadoQuestaoA ?: "",
        enunciadoQuestaoB = enunciadoQuestaoB ?: "",
        enunciadoQuestaoC = enunciadoQuestaoC ?: "",
        enunciadoQuestaoD = enunciadoQuestaoD ?: "",
        enunciadoQuestaoE = enunciadoQuestaoE ?: "",
        imagemEnunciado = imagemEnunciado ?: "",
        respostaQuestao = respostaQuestao ?: "",
        exercicioTipoSeis = exercicioTipoSeis?.mapValues { it.value.toDomain() }
    )
}

fun QuestaoPathResponse.toDomain(): QuestaoPathData {
    return QuestaoPathData(
        imagem = imagem,
        idExercicio = idExercicio,
        id = id,
        resposta = resposta
    )
}