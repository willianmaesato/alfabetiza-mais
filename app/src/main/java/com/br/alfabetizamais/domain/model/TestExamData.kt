package com.br.alfabetizamais.domain.model

import android.os.Parcelable
import androidx.compose.ui.graphics.Color
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TestExamData(
    val idProva: Int,
    val mediaProva: Int,
    val valorProva: Int,
    val titleSchoolSubject: String,
    val exerciseSchoolSubjectTitle: String,
    val lightColor: Color,
    val mediumColor: Color,
    val highColor: Color,
    val image: Int
) : Parcelable