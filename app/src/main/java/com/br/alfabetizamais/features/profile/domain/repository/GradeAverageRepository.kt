package com.br.alfabetizamais.features.profile.domain.repository

import com.br.alfabetizamais.features.profile.domain.model.GradeAverageTestExamData
import kotlinx.coroutines.flow.Flow

interface GradeAverageRepository {
    fun getListGradeAverage(idAluno: Int): Flow<List<GradeAverageTestExamData>>
}