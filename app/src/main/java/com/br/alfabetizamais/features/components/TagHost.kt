package com.br.alfabetizamais.features.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.br.alfabetizamais.ui.theme.HighPurple
import com.br.alfabetizamais.ui.theme.MediumPurple

@Composable
fun TagHost(
    modifier: Modifier = Modifier,
    isSelected: Boolean,
    text: String,
    onClick: () -> Unit
) {

    Box(
        modifier = modifier
            .padding(8.dp)
            .height(40.dp)
            .width(60.dp)
            .clip(RoundedCornerShape(8.dp))
            .background(if (isSelected) HighPurple else MediumPurple)
            .clickable {
                onClick()
            },
    ) {
        Text(
            text = text,
            color = Color.White,
            textAlign = TextAlign.Center,
            modifier = Modifier.align(
                Alignment.Center
            )
        )
    }
}