package com.br.alfabetizamais.features.components

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.br.alfabetizamais.features.profile.domain.model.GradeAverage
import com.br.alfabetizamais.features.profile.domain.model.GradeAverageTestExamData
import com.br.alfabetizamais.ui.theme.*
import com.br.alfabetizamais.util.formatDate

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun GradeAverageTestExamItem(
    item: GradeAverageTestExamData
) {
    val listColor =
        when (item.resultado) {
            GradeAverage.ABOVE_AVERAGE -> listOf(HighBlue, MediumBlue, LightBlue)
            GradeAverage.BELOW_AVERAGE -> listOf(HighRed, MediumRed, LightRed)
            GradeAverage.IN_AVERAGE -> listOf(HighAmber, MediumAmber, LightAmber)
            GradeAverage.FULL_MARK -> listOf(HighGreen, MediumGreen, LightGreen)
        }

    BoxWithConstraints(
        modifier = Modifier
            .padding(12.dp)
            .aspectRatio(1f)
            .clip(RoundedCornerShape(10.dp))
            .background(listColor.first())
    ) {
        val width = constraints.maxWidth
        val height = constraints.maxHeight

        val mediumColoredPoint1 = Offset(0f, height * 0.3f)
        val mediumColoredPoint2 = Offset(width * 0.1f, height * 0.35f)
        val mediumColoredPoint3 = Offset(width * 0.4f, height * 0.05f)
        val mediumColoredPoint4 = Offset(width * 0.75f, height * 0.7f)
        val mediumColoredPoint5 = Offset(width * 1.4f, -height.toFloat())

        val mediumColoredPath = Path().apply {
            moveTo(mediumColoredPoint1.x, mediumColoredPoint1.y)
            standardQuadFromTo(mediumColoredPoint1, mediumColoredPoint2)
            standardQuadFromTo(mediumColoredPoint2, mediumColoredPoint3)
            standardQuadFromTo(mediumColoredPoint3, mediumColoredPoint4)
            standardQuadFromTo(mediumColoredPoint4, mediumColoredPoint5)
            lineTo(width.toFloat() + 100f, height.toFloat() + 100f)
            lineTo(-100f, height.toFloat() + 100f)
            close()
        }

        val lightPoint1 = Offset(0f, height * 0.35f)
        val lightPoint2 = Offset(width * 0.1f, height * 0.4f)
        val lightPoint3 = Offset(width * 0.3f, height * 0.35f)
        val lightPoint4 = Offset(width * 0.65f, height.toFloat())
        val lightPoint5 = Offset(width * 1.4f, -height.toFloat() / 3f)

        val lightColoredPath = Path().apply {
            moveTo(lightPoint1.x, lightPoint1.y)
            standardQuadFromTo(lightPoint1, lightPoint2)
            standardQuadFromTo(lightPoint2, lightPoint3)
            standardQuadFromTo(lightPoint3, lightPoint4)
            standardQuadFromTo(lightPoint4, lightPoint5)
            lineTo(width.toFloat() + 100f, height.toFloat() + 100f)
            lineTo(-100f, height.toFloat() + 100f)
            close()
        }

        Canvas(
            modifier = Modifier
                .fillMaxSize()
        ) {
            drawPath(
                path = mediumColoredPath,
                color = listColor[1]
            )
            drawPath(
                path = lightColoredPath,
                color = listColor[2]
            )
        }
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(8.dp)
        ) {
            Text(
                text = item.titulo,
                style = MaterialTheme.typography.body1,
                fontWeight = FontWeight.Bold,
                color = Color.White,
                fontSize = 18.sp,
                maxLines = 3,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier
                    .align(Alignment.TopStart)
            )
            Text(
                text = "Nota: ${item.valorProva}\n" +
                        "Média: ${item.media * 10}%\n" +
                        "Data: ${formatDate(item.dataFinalizacao)}",
                style = MaterialTheme.typography.body1,
                fontWeight = FontWeight.Bold,
                color = Color.White,
                fontSize = 18.sp,
                modifier = Modifier
                    .align(Alignment.BottomStart)
            )
        }
    }
}