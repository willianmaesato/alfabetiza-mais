package com.br.alfabetizamais.features.profile

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.br.alfabetizamais.data.PreferencesManager
import com.br.alfabetizamais.features.profile.use_case.GradeAverageUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GradeAverageViewModel @Inject constructor(
    private val preferencesManager: PreferencesManager,
    private val gradeAverageUseCase: GradeAverageUseCase
) : ViewModel() {
    var state by mutableStateOf(GradeAverageState())

    init {
        getUserLogged()
    }

    private fun getUserLogged() {
        viewModelScope.launch {
            preferencesManager.userDataInit
                .flowOn(Dispatchers.IO)
                .onStart { showLoading() }
                .catch { hideLoading() }
                .onCompletion { hideLoading() }
                .collect { user ->
                    getListGradleTestExam(user.id)
                }
        }
    }

    private fun getListGradleTestExam(idAluno: Int) {
        viewModelScope.launch {
            gradeAverageUseCase.invoke(idAluno)
                .flowOn(Dispatchers.IO)
                .onStart { showLoading() }
                .catch { hideLoading() }
                .onCompletion { hideLoading() }
                .collect { listGradeAverage ->
                    state = state.copy(list = listGradeAverage)
                }
        }
    }

    private fun showLoading() {
        state = state.copy(isLoading = true)
    }

    private fun hideLoading() {
        state = state.copy(isLoading = false)
    }

}