package com.br.alfabetizamais.features.enter.presenter.login

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.br.alfabetizamais.R
import com.br.alfabetizamais.features.components.RoundedButton
import com.br.alfabetizamais.features.components.StandardOutlinedTextField
import com.br.alfabetizamais.navigation.Screen
import com.br.alfabetizamais.ui.theme.*
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import kotlinx.coroutines.flow.collect

@ExperimentalComposeUiApi
@Composable
fun LoginScreen(navController: NavHostController, viewModel: LoginViewModel = hiltViewModel()) {

    val focusManager = LocalFocusManager.current

    val state = viewModel.state
    val systemUiController = rememberSystemUiController()
    SideEffect {
        systemUiController.setSystemBarsColor(
            color = MediumPurple
        )
    }

    val context = LocalContext.current
    LaunchedEffect(key1 = context) {
        viewModel.validationEvents.collect { event ->
            when (event) {
                LoginEvent.Success -> {
                    navController.popBackStack(Screen.EnterScreen.route, true)
                    navController.navigate(Screen.HomeScreen.route)
                }
                LoginEvent.Error -> {
                    Toast.makeText(context, "E-mail ou senha incorretos", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    Scaffold(topBar = {
        TopAppBar(
            title = { Text(text = "Login", color = MediumPurple) },
            navigationIcon = {
                IconButton(onClick = { navController.popBackStack() }) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = null,
                        tint = Color.Black
                    )
                }
            },
            backgroundColor = Color.White,
            elevation = 0.dp
        )
    }, backgroundColor = Color.White) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(paddingMedium)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(paddingMedium)
                    .align(Alignment.Center), verticalArrangement = Arrangement.Top
            ) {
                StandardOutlinedTextField(
                    text = state.user, onValueChange = {
                        viewModel.changeEmail(it)
                    },
                    error = state.userError,
                    hint = stringResource(id = R.string.name_hint),
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Next,
                    keyBoardActions = KeyboardActions(
                        onNext = {
                            focusManager.moveFocus(FocusDirection.Down)
                        }
                    )
                )
                Spacer(modifier = Modifier.height(paddingSmall))
                StandardOutlinedTextField(
                    text = state.password,
                    onValueChange = {
                        viewModel.changePassword(it)
                    },
                    error = state.passwordError,
                    hint = stringResource(id = R.string.password_hint),
                    keyboardType = KeyboardType.Password,
                    imeAction = ImeAction.Done,
                    keyBoardActions = KeyboardActions(
                        onDone = {
                            focusManager.clearFocus()
                            viewModel.submitData()
                        }
                    )
                )
                Spacer(modifier = Modifier.height(paddingHigh))
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    RoundedButton(
                        text = stringResource(id = R.string.sign_in),
                        onClick = {
                            viewModel.submitData()
                        },
                        displayProgressBar = state.isLoading,
                        buttonColors = ButtonDefaults.buttonColors(
                            backgroundColor = MediumPurple,
                            contentColor = Color.White
                        ),
                        colorText = Color.White
                    )
                }
            }
        }
    }
}