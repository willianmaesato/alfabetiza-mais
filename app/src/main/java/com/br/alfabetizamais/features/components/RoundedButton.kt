package com.br.alfabetizamais.features.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.br.alfabetizamais.ui.theme.LightPurple

@Composable
fun RoundedButton(
    modifier: Modifier = Modifier,
    text: String,
    displayProgressBar: Boolean = false,
    onClick: () -> Unit,
    buttonElevation: ButtonElevation = ButtonDefaults.elevation(),
    buttonColors: ButtonColors = ButtonDefaults.buttonColors(contentColor = LightPurple),
    colorText: Color = Color.White,
    isEnable: Boolean = true,
) {
    Button(
        modifier = modifier
            .fillMaxWidth()
            .height(50.dp),
        onClick = onClick,
        shape = RoundedCornerShape(16),
        elevation = buttonElevation,
        colors = buttonColors,
        enabled = isEnable
    ) {
        if (!displayProgressBar) {
            Text(
                text = text,
                style = MaterialTheme.typography.h6.copy(
                    color = colorText
                )
            )
        } else {
            CircularProgressIndicator(
                modifier = Modifier.size(30.dp),
                color = Color.White,
                strokeWidth = 6.dp
            )
        }
    }
}