package com.br.alfabetizamais.features.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.br.alfabetizamais.data.PreferencesManager
import com.br.alfabetizamais.util.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val preferencesManager: PreferencesManager
) : ViewModel() {

    private val validationEventChannel = Channel<SplashEvent>()
    val validationEvents = validationEventChannel.receiveAsFlow()

    init {
        viewModelScope.launch {
            preferencesManager.isLogged
                .flowOn(Dispatchers.IO)
                .onStart { }
                .catch { }
                .onCompletion { }.collect { isLogged ->
                    delay(Constants.SPLASH_SCREEN_DURATION)
                    if (isLogged) {
                        validationEventChannel.send(SplashEvent.HomeScreen)
                    } else {
                        validationEventChannel.send(SplashEvent.EnterScreen)
                    }
                }
        }
    }
}