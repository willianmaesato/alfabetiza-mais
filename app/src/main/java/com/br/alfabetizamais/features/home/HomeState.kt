package com.br.alfabetizamais.features.home

import com.br.alfabetizamais.domain.model.TestExamData

data class HomeState(
    val list: List<TestExamData> = emptyList(),
    val isLoading: Boolean = false,
    val search: String = "",
    val userName: String = "",
    val idAluno: Int = 0
)