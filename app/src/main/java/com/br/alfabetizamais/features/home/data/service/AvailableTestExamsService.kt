package com.br.alfabetizamais.features.home.data.service

import com.br.alfabetizamais.features.home.data.model.AvailableTestExamsResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface AvailableTestExamsService {

    @GET("prova/provasdisponiveisporturma/{idTurma}")
    suspend fun getAvailableTestExams(@Path("idTurma") idTurma: Int): List<AvailableTestExamsResponse>
}