package com.br.alfabetizamais.features.splash

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.res.painterResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.br.alfabetizamais.R
import com.br.alfabetizamais.navigation.Screen
import kotlinx.coroutines.flow.collect

@Composable
fun SplashScreen(navController: NavController, viewModel: SplashViewModel = hiltViewModel()) {
    LaunchedEffect(key1 = true, block = {
        viewModel.validationEvents.collect { event ->
            when (event) {
                SplashEvent.HomeScreen -> {
                    navController.popBackStack()
                    navController.navigate(Screen.HomeScreen.route)
                }
                SplashEvent.EnterScreen -> {
                    navController.popBackStack()
                    navController.navigate(Screen.EnterScreen.route)
                }
            }
        }
    }
    )
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_logo),
            contentDescription = "Logo",
            modifier = Modifier.scale(1f)
        )
    }
}