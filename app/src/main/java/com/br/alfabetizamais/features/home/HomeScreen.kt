package com.br.alfabetizamais.features.home

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.br.alfabetizamais.R
import com.br.alfabetizamais.features.components.CustomDialogUI
import com.br.alfabetizamais.features.components.CustomTopAppBar
import com.br.alfabetizamais.features.components.StandardTextFieldBar
import com.br.alfabetizamais.features.components.TestExamItem
import com.br.alfabetizamais.navigation.Screen
import com.br.alfabetizamais.ui.theme.LightGray
import com.br.alfabetizamais.ui.theme.MediumPurple
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import kotlinx.coroutines.flow.collect

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun HomeScreen(navController: NavController, viewModel: HomeViewModel = hiltViewModel()) {

    val systemUiController = rememberSystemUiController()
    SideEffect {
        systemUiController.setSystemBarsColor(
            color = MediumPurple
        )
    }

    val scaffoldState = rememberScaffoldState()
    val openDialogCustom = remember { mutableStateOf(false) }
    val state = viewModel.state

    val context = LocalContext.current
    LaunchedEffect(key1 = context) {
        viewModel.validationEvents.collect { event ->
            when (event) {
                HomeEvent.Logout -> {
                    navController.popBackStack()
                    navController.navigate(Screen.EnterScreen.route)
                }
            }
        }
    }

    Scaffold(
        backgroundColor = LightGray,
        topBar = {
            CustomTopAppBar("Olá, ${state.userName}", isEnableLogoutButton = true, onClickLogoutButton = {
                openDialogCustom.value = true
            })
        },
        scaffoldState = scaffoldState
    ) {
        Card(
            backgroundColor = Color.White,
            shape = RoundedCornerShape(topEnd = 40.dp, topStart = 40.dp),
            modifier = Modifier.fillMaxSize(),
            elevation = 0.dp
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    StandardTextFieldBar(
                        state.search,
                        hint = "Pesquisar matéria",
                        onValueChange = {
                            viewModel.searchTestExam(it)
                        })
                }
                if (state.isLoading) {
                    Column(
                        modifier = Modifier.fillMaxSize(),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        CircularProgressIndicator()
                    }
                } else {
                    if (state.list.isEmpty()) {
                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .wrapContentHeight()
                                .padding(vertical = 25.dp),
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.SpaceAround
                        ) {
                            Spacer(modifier = Modifier.height(50.dp))
                            Image(
                                painter = painterResource(id = R.drawable.ic_books),
                                contentDescription = "Book",
                                modifier = Modifier
                                    .width(120.dp)
                                    .height(120.dp)
                            )
                            Spacer(modifier = Modifier.height(16.dp))
                            Text(
                                "Nenhuma prova disponivel ainda \uD83D\uDE2D",
                                style = MaterialTheme.typography.h1,
                                textAlign = TextAlign.Center
                            )
                        }
                    } else {
                        Text(
                            text = "Provas disponíveis:", style = MaterialTheme.typography.body1,
                            color = Color.Black,
                            fontSize = 26.sp, modifier = Modifier.padding(start = 16.dp)
                        )
                        Spacer(modifier = Modifier.height(16.dp))
                        LazyVerticalGrid(
                            cells = GridCells.Fixed(2),
                            contentPadding = PaddingValues(
                                start = 7.5.dp,
                                end = 7.5.dp,
                                bottom = 70.dp
                            ),
                            modifier = Modifier.fillMaxHeight()
                        ) {
                            items(state.list) { item ->
                                TestExamItem(item) {
                                    navController.currentBackStackEntry?.savedStateHandle?.set(
                                        "idAluno",
                                        state.idAluno
                                    )
                                    navController.currentBackStackEntry?.savedStateHandle?.set(
                                        "testExamItem",
                                        item
                                    )
                                    navController.navigate(Screen.TestExamsToDoScreen.route)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if (openDialogCustom.value) {
        Dialog(onDismissRequest = { openDialogCustom.value = false }) {
            CustomDialogUI(openDialogCustom = openDialogCustom, title = "${state.userName}, deseja realmente sair", onClickYes = {
                viewModel.logoutApp()
            })
        }
    }
}