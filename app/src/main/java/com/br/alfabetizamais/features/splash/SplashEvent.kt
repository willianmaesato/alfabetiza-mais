package com.br.alfabetizamais.features.splash

sealed class SplashEvent {
    object HomeScreen : SplashEvent()
    object EnterScreen : SplashEvent()
}