package com.br.alfabetizamais.features.enter.presenter.login

sealed class LoginEvent {
    object Success : LoginEvent()
    object Error : LoginEvent()
}