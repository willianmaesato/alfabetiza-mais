package com.br.alfabetizamais.features.profile

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.br.alfabetizamais.R
import com.br.alfabetizamais.features.components.CustomTopAppBar
import com.br.alfabetizamais.features.components.GradeAverageTestExamItem
import com.br.alfabetizamais.ui.theme.LightGray

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun GradleTestExamsScreen(
    navController: NavController,
    viewModel: GradeAverageViewModel = hiltViewModel()
) {

    val state = viewModel.state

    val scaffoldState = rememberScaffoldState()

    Scaffold(
        backgroundColor = LightGray,
        topBar = {
            CustomTopAppBar("Notas")
        },
        scaffoldState = scaffoldState
    ) {
        Card(
            backgroundColor = Color.White,
            shape = RoundedCornerShape(topEnd = 40.dp, topStart = 40.dp),
            modifier = Modifier.fillMaxSize(),
            elevation = 0.dp
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White)
            ) {
                if (state.isLoading) {
                    Column(
                        modifier = Modifier.fillMaxSize(),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        CircularProgressIndicator()
                    }
                } else {
                    if (state.list.isEmpty()) {
                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .wrapContentHeight()
                                .padding(vertical = 25.dp),
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.SpaceAround
                        ) {
                            Spacer(modifier = Modifier.height(50.dp))
                            Image(
                                painter = painterResource(id = R.drawable.ic_books),
                                contentDescription = "Book",
                                modifier = Modifier
                                    .width(120.dp)
                                    .height(120.dp)
                            )
                            Spacer(modifier = Modifier.height(16.dp))
                            Text(
                                "Nenhuma prova disponivel ainda \uD83D\uDE2D",
                                style = MaterialTheme.typography.h1,
                                textAlign = TextAlign.Center
                            )
                        }
                    } else {
                        Text(
                            text = "Nota das provas:",
                            style = MaterialTheme.typography.body1,
                            color = Color.Black,
                            fontSize = 26.sp,
                            modifier = Modifier.padding(start = 16.dp, top = 16.dp)
                        )
                        Spacer(modifier = Modifier.height(16.dp))
                        LazyVerticalGrid(
                            cells = GridCells.Fixed(2),
                            contentPadding = PaddingValues(
                                start = 7.5.dp,
                                end = 7.5.dp,
                                bottom = 70.dp
                            ),
                            modifier = Modifier.fillMaxHeight()
                        ) {
                            items(state.list) { item ->
                                GradeAverageTestExamItem(item)
                            }
                        }
                    }
                }
            }
        }
    }
}