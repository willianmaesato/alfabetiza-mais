package com.br.alfabetizamais.features.enter.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class LoginData(
    val user: String,
    val password: String
)
