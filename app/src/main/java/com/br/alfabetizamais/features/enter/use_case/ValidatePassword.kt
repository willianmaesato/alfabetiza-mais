package com.br.alfabetizamais.features.enter.use_case

import com.br.alfabetizamais.features.enter.domain.model.ValidationResult

class ValidatePassword {

    fun execute(password: String): ValidationResult {
        if (password.length < 6) {
            return ValidationResult(
                successful = false,
                errorMessage = "A senha precisa de pelo menos 8 caracteres"
            )
        }
        if (password.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "A senha não pode ser em branco"
            )
        }

        return ValidationResult(
            successful = true,
            errorMessage = null
        )
    }
}