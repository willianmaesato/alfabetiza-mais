package com.br.alfabetizamais.features.home.domain.mapper

import com.br.alfabetizamais.features.home.data.model.AvailableTestExamsResponse
import com.br.alfabetizamais.features.home.data.model.TestExamResponse
import com.br.alfabetizamais.features.home.domain.model.AvailableTestExamsData
import com.br.alfabetizamais.features.home.domain.model.TestExamData


fun AvailableTestExamsResponse.toDomain(): AvailableTestExamsData {
    return AvailableTestExamsData(
        prova = prova.toDomain()
    )
}

fun TestExamResponse.toDomain(): TestExamData {
    return TestExamData(
        id = id,
        titulo = titulo,
        media = media,
        valorProva = valorProva,
        dataLiberacao = dataLiberacao,
        dateEncerramento = dateEncerramento,
        quantidadeTentativas = quantidadeTentativas
    )
}