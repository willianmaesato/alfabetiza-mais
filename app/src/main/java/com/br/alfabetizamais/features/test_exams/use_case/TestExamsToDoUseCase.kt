package com.br.alfabetizamais.features.test_exams.use_case

import com.br.alfabetizamais.data.model.NotaResponse
import com.br.alfabetizamais.domain.model.GradeTestExamData
import com.br.alfabetizamais.domain.model.TestExamsData
import com.br.alfabetizamais.features.home.domain.model.AvailableTestExamsData
import com.br.alfabetizamais.features.test_exams.domain.repository.TestExamsToDoRepository
import kotlinx.coroutines.flow.Flow

class TestExamsToDoUseCase(private val testExamsToDoRepository: TestExamsToDoRepository) {
    fun invoke(idProva: Int): Flow<List<TestExamsData>> =
        testExamsToDoRepository.getListTestExams(idProva)

    fun sendTestExam(gradeTestExamData: GradeTestExamData): Flow<NotaResponse> =
        testExamsToDoRepository.sendGradeTestExam(gradeTestExamData)
}