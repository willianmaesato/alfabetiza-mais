package com.br.alfabetizamais.features.profile.domain.model

import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Serializable
data class GradeAverageTestExamData(
    val nota: Double,
    val dataFinalizacao: String,
    val titulo: String,
    val media: Long,
    val valorProva: Long,
    val resultado: GradeAverage
)

@Keep
enum class GradeAverage {
    ABOVE_AVERAGE, BELOW_AVERAGE, IN_AVERAGE, FULL_MARK;

    companion object {
        fun getDocumentType(resultado: String): GradeAverage {
            return when {
                resultado.contains("Maxima") -> FULL_MARK
                resultado.contains("Acima") -> ABOVE_AVERAGE
                resultado.contains("Abaixo") -> BELOW_AVERAGE
                else -> IN_AVERAGE
            }
        }
    }
}