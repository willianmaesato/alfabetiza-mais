package com.br.alfabetizamais.features.enter.domain.repositorry

import com.br.alfabetizamais.domain.model.UserData
import com.br.alfabetizamais.features.enter.domain.model.LoginData
import kotlinx.coroutines.flow.Flow

interface LoginRepository {
    fun login(login: LoginData) : Flow<UserData>
}