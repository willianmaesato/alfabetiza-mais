package com.br.alfabetizamais.features.enter.domain.mapper

import com.br.alfabetizamais.features.enter.data.model.LoginResponse
import com.br.alfabetizamais.features.enter.domain.model.LoginData

fun LoginData.toRemote(): LoginResponse {
    return LoginResponse(
        user = user,
        password = password
    )
}