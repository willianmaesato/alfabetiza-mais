package com.br.alfabetizamais.features.profile.data.repository

import com.br.alfabetizamais.features.profile.data.service.GradeAverageService
import com.br.alfabetizamais.features.profile.domain.mapper.toDomain
import com.br.alfabetizamais.features.profile.domain.model.GradeAverageTestExamData
import com.br.alfabetizamais.features.profile.domain.repository.GradeAverageRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GradeAverageRepositoryImpl(private val service: GradeAverageService) :
    GradeAverageRepository {
    override fun getListGradeAverage(idAluno: Int): Flow<List<GradeAverageTestExamData>> = flow {
        emit(service.getListGradeAverage(idAluno).map { it.toDomain() })
    }
}