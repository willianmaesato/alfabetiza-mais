package com.br.alfabetizamais.features.enter.data.service

import com.br.alfabetizamais.data.model.GenericResponse
import com.br.alfabetizamais.features.enter.data.model.LoginResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService {
    @POST("login/aluno")
    suspend fun login(@Body login: LoginResponse): GenericResponse
}