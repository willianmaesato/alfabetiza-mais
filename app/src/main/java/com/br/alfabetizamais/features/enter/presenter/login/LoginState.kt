package com.br.alfabetizamais.features.enter.presenter.login

data class LoginState(
    val user: String = "",
    val userError: String? = null,
    val password: String = "",
    val passwordError: String? = null,
    val isLoading: Boolean = false
)