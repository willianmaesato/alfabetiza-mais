package com.br.alfabetizamais.features.test_exams.to_do

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.br.alfabetizamais.domain.model.*
import com.br.alfabetizamais.features.test_exams.domain.*
import com.br.alfabetizamais.features.test_exams.use_case.TestExamsToDoUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TestExamsToDoViewModel @Inject constructor(
    private val testExamsToDoUseCase: TestExamsToDoUseCase
) : ViewModel() {

    var state by mutableStateOf(TestExamsToDoState())

    private var exercises: ArrayList<AnswersData> = arrayListOf()

    private var firstQuestionType: List<MultipleWithOneAnswer> = emptyList()
    private var secondQuestionType: List<MultipleWithOneAnswer> = emptyList()
    private var thirdQuestionType: String = ""
    private var fourthQuestionType: MathAnswer = MathAnswer("", "")
    private var fifthQuestionType: ArrayList<MultipleWithOneAnswer> = arrayListOf()
    private var sixthQuestionType: ArrayList<PathAnswer> = arrayListOf()

    private var isFirst: Boolean = true

    private var notaCreate: Double = 0.0
    private var notaPath: Double = 0.0

    private var idTestExam: Int = 0
    private var idAlunoAux: Int = 0
    private var testExamData: TestExamData? = null

    private var hasExerciseTypeThree: Boolean = false

    private val validationEventChannel = Channel<TestExamsEvent>()
    val validationEvents = validationEventChannel.receiveAsFlow()

    fun getStepByStep(): String =
        "${state.stepTestExams + 1}/${state.testExam.size}"

    fun getTextButton(): String =
        when {
            state.gradeTestExamData != null -> "Sair"
            state.isError -> "Tentar novamente"
            state.isLastQuestion -> "Finalizar"
            else -> "Proximo"
        }

    fun isEnableButton(): Boolean =
        state.isEnableButton || state.isError

    fun setTestExam(testExam: TestExamData, idAluno: Int) {
        if (isFirst) {
            isFirst = true
            testExamData = testExam
            idAlunoAux = idAluno
            viewModelScope.launch {
                testExamsToDoUseCase.invoke(testExam.idProva)
                    .flowOn(Dispatchers.IO)
                    .onStart {
                        showLoading()
                        hideError()
                    }
                    .catch {
                        hideLoading()
                        showError()
                    }
                    .onCompletion { hideLoading() }
                    .collect(::handleTestExam)
            }
        }
    }

    private fun handleTestExam(testExams: List<TestExamsData>) {
        if (isFirst) {
            state = state.copy(testExam = testExams, isLoading = false)
            isFirst = false
            setExerciseByStep()
        }
    }

    fun getTitleTestExam(): String =
        when {
            state.isError -> "Error"
            state.isLoading -> "....."
            else -> state.testExam[state.stepTestExams].prova.exercicio.name ?: "Prova"

        }

    fun nextTestExams() {
        if (state.isError) {
            isFirst = true
            setTestExam(testExamData!!, idAlunoAux)
        } else if (state.gradeTestExamData != null) {
            exit()
        } else {
            if (!state.isEnableButton) {
                return
            }
            setAnswersInList(state.testExam[state.stepTestExams].prova)
            if (isLastQuestion()) {
                sendAnswers()
            } else {
                state = state.copy(stepTestExams = state.stepTestExams.plus(1))
                state = state.copy(isLastQuestion = isLastQuestion())
                setExerciseByStep()
            }
        }
    }

    private fun isLastQuestion(): Boolean {
        return state.stepTestExams == state.testExam.size - 1
    }

    private fun setToCreateAnswers(exercicioData: ExercicioData) {
        when (exercicioData.tipoExercicio) {
            1 -> firstQuestionType(exercicioData)
            2 -> secondQuestionType(exercicioData)
            3 -> thirdQuestionType()
            4 -> fourthQuestionType(exercicioData)
            5 -> fifthQuestionType(exercicioData)
            6 -> sixthQuestionType(exercicioData)
        }
    }

    private fun sixthQuestionType(exercicioData: ExercicioData) {
        sixthQuestionType.clear()
        exercicioData.exercicioTipoSeis!!.forEach { questaoPathData ->
            sixthQuestionType.add(
                PathAnswer(
                    id = questaoPathData.value.id,
                    idExercise = questaoPathData.value.idExercicio,
                    path = questaoPathData.value.imagem,
                    answer = questaoPathData.value.resposta,
                    answerAsked = ""
                )
            )
        }
        state = state.copy(sixthQuestionType = sixthQuestionType, isEnableButton = false)
    }

    private fun fifthQuestionType(exercicioData: ExercicioData) {
        fifthQuestionType.clear()
        val numRange =
            exercicioData.numeroMinimo!!..exercicioData.numeroMaximo!!
        numRange.forEach {
            fifthQuestionType.add(
                MultipleWithOneAnswer(
                    title = "$it",
                    selected = false,
                    answer = "$it"
                )
            )
        }
        state = state.copy(fifthQuestionType = fifthQuestionType.toList(), isEnableButton = false)
    }

    private fun fourthQuestionType(exercicioData: ExercicioData) {
        fourthQuestionType = MathAnswer("", "")
        fourthQuestionType = MathAnswer(
            mathDescription = "${exercicioData.numeroA}\n${exercicioData.operacao}${exercicioData.numeroB}",
            answerToMath = ""
        )
        state = state.copy(fourthQuestionType = fourthQuestionType, isEnableButton = false)
    }

    private fun firstQuestionType(exercicioData: ExercicioData) {
        firstQuestionType = listOf(
            MultipleWithOneAnswer(
                title = exercicioData.enunciadoQuestaoA!!,
                false,
                "A"
            ),
            MultipleWithOneAnswer(
                title = exercicioData.enunciadoQuestaoB!!,
                false,
                "B"
            ),
            MultipleWithOneAnswer(
                title = exercicioData.enunciadoQuestaoC!!,
                false,
                "C"
            ),
            MultipleWithOneAnswer(
                title = exercicioData.enunciadoQuestaoD!!,
                false,
                "D"
            ),
            MultipleWithOneAnswer(
                title = exercicioData.enunciadoQuestaoE!!,
                false,
                "E"
            ),
        )
        state = state.copy(
            firstQuestionType = firstQuestionType, isEnableButton = false
        )
    }

    private fun secondQuestionType(exercicioData: ExercicioData) {
        secondQuestionType = listOf(
            MultipleWithOneAnswer(
                title = exercicioData.enunciadoQuestaoA!!,
                false,
                "A"
            ),
            MultipleWithOneAnswer(
                title = exercicioData.enunciadoQuestaoB!!,
                false,
                "B"
            ),
            MultipleWithOneAnswer(
                title = exercicioData.enunciadoQuestaoC!!,
                false,
                "C"
            ),
            MultipleWithOneAnswer(
                title = exercicioData.enunciadoQuestaoD!!,
                false,
                "D"
            ),
            MultipleWithOneAnswer(
                title = exercicioData.enunciadoQuestaoE!!,
                false,
                "E"
            ),
        )
        state = state.copy(
            secondQuestionType = secondQuestionType, isEnableButton = false
        )
    }

    private fun thirdQuestionType() {
        hasExerciseTypeThree = true
        thirdQuestionType = ""
        state = state.copy(thirdQuestionType = "", isEnableButton = false)
    }

    fun setAnswersForFirstType(isSelected: Boolean, item: MultipleWithOneAnswer) {
        state = state.copy(firstQuestionType = emptyList())
        firstQuestionType.forEach {
            if (it == item) {
                it.selected = isSelected
            } else {
                it.selected = false
            }
        }
        val isEnable = firstQuestionType.find { it.selected }?.selected ?: false
        state = state.copy(firstQuestionType = firstQuestionType, isEnableButton = isEnable)
    }

    fun setAnswersForSecondType(isSelected: Boolean, item: MultipleWithOneAnswer) {
        state = state.copy(secondQuestionType = emptyList())
        secondQuestionType.forEach {
            if (it == item) {
                it.selected = isSelected
            }
        }
        val isEnable = secondQuestionType.find { it.selected }?.selected ?: false
        state = state.copy(secondQuestionType = secondQuestionType, isEnableButton = isEnable)
    }


    fun setAnswersForThirdType(text: String) {
        state = state.copy(thirdQuestionType = text, isEnableButton = text.trim().isNotEmpty())
    }

    fun setAnswersForFourthType(answer: String) {
        state = state.copy(fourthQuestionType = MathAnswer("", ""))
        fourthQuestionType.answerToMath = answer
        state = state.copy(
            fourthQuestionType = fourthQuestionType,
            isEnableButton = answer.trim().isNotEmpty()
        )
    }

    fun setAnswersForFifthType(item: MultipleWithOneAnswer) {
        state = state.copy(fifthQuestionType = emptyList())
        fifthQuestionType.forEach {
            if (it == item) {
                it.selected = !it.selected
            }
        }
        val isEnable = fifthQuestionType.find { it.selected }?.selected ?: false
        state = state.copy(fifthQuestionType = fifthQuestionType, isEnableButton = isEnable)
    }

    fun setAnswersForSixthType(text: String, item: PathAnswer) {
        state = state.copy(sixthQuestionType = emptyList())
        sixthQuestionType.forEach {
            if (it == item) {
                it.answerAsked = text
            }
        }
        val isEnable = sixthQuestionType.all { it.answerAsked.trim().isNotEmpty() }
        state = state.copy(sixthQuestionType = sixthQuestionType, isEnableButton = isEnable)
    }


    private fun setExerciseByStep() {
        state = state.copy(exerciseByStep = state.testExam[state.stepTestExams].prova.exercicio)
        setToCreateAnswers(state.testExam[state.stepTestExams].prova.exercicio)
    }

    private fun setAnswersInList(prova: ProvaData) {
        when (prova.exercicio.tipoExercicio!!) {
            1 -> {
                val answerList = firstQuestionType.find { it.selected }!!
                exercises.add(
                    AnswersData(
                        valorQuestao = prova.valorExercicio,
                        idExercicio = prova.idExercicio,
                        resposta = answerList.answer,
                        status = prova.exercicio.respostaQuestao.contentEquals(answerList.answer),
                        questao = emptyList()
                    )
                )
            }
            2 -> {
                val answerList = secondQuestionType.filter { it.selected }
                var answer = ""
                answerList.forEach {
                    answer += if (answer.isEmpty()) {
                        it.answer
                    } else {
                        "@${it.answer}"
                    }
                }
                exercises.add(
                    AnswersData(
                        valorQuestao = prova.valorExercicio,
                        idExercicio = prova.idExercicio,
                        resposta = answer,
                        status = prova.exercicio.respostaQuestao.contentEquals(answer),
                        questao = emptyList()
                    )
                )
            }
            3 -> {
                exercises.add(
                    AnswersData(
                        valorQuestao = prova.valorExercicio,
                        idExercicio = prova.idExercicio,
                        resposta = thirdQuestionType,
                        status = false,
                        questao = emptyList()
                    )
                )
            }
            4 -> {
                val answer = fourthQuestionType.answerToMath
                exercises.add(
                    AnswersData(
                        valorQuestao = prova.valorExercicio,
                        idExercicio = prova.idExercicio,
                        resposta = answer,
                        status = prova.exercicio.respostaQuestao.contentEquals(answer),
                        questao = emptyList()
                    )
                )
            }
            5 -> {
                val answerList = fifthQuestionType.filter { it.selected }
                var answer = ""
                answerList.forEach {
                    answer += if (answer.isEmpty()) {
                        it.answer
                    } else {
                        "@${it.answer}"
                    }
                }
                exercises.add(
                    AnswersData(
                        valorQuestao = prova.valorExercicio,
                        idExercicio = prova.idExercicio,
                        resposta = answer,
                        status = prova.exercicio.respostaQuestao.contentEquals(answer),
                        questao = emptyList()
                    )
                )
            }
            6 -> {
                val list: ArrayList<AskData> = arrayListOf()
                sixthQuestionType.forEach {
                    list.add(
                        AskData(
                            it.answer.contentEquals(it.answerAsked),
                            resposta = it.answerAsked
                        )
                    )
                }
                exercises.add(
                    AnswersData(
                        valorQuestao = prova.valorExercicio,
                        idExercicio = prova.idExercicio,
                        resposta = "",
                        status = true,
                        questao = list
                    )
                )
            }
        }
    }

    private fun sendAnswers() {
        state = state.copy(isShowProgressBar = true)
        exercises.forEach { exercise ->
            if (exercise.status) {
                if (exercise.questao?.isNullOrEmpty() == false) {
                    notaPath = (exercise.valorQuestao.toDouble() / exercise.questao.size.toDouble())
                    exercise.questao.forEach { pathQuest ->
                        if (pathQuest.status) {
                            notaCreate += notaPath
                        }
                    }
                } else {
                    notaCreate += exercise.valorQuestao.toDouble()
                }
            }
        }

        viewModelScope.launch {
            val gradeTestExam = GradeTestExamData(
                nota = notaCreate,
                idProva = idTestExam,
                idAluno = idAlunoAux,
                exercisethree = if (hasExerciseTypeThree) 1 else 0,
                valorProva = testExamData!!.valorProva,
                mediaProva = ((testExamData!!.mediaProva.toDouble() * 10) / testExamData!!.valorProva.toDouble())
            )
            testExamsToDoUseCase.sendTestExam(gradeTestExam)
                .flowOn(Dispatchers.IO)
                .collect {
                    state = state.copy(
                        gradeTestExamData = gradeTestExam,
                        isShowProgressBar = false
                    )
                }
        }
    }

    fun exit() {
        viewModelScope.launch {
            if (state.isError || state.gradeTestExamData != null) {
                validationEventChannel.send(TestExamsEvent.ExitWithErrorTestExam)
            } else {
                validationEventChannel.send(TestExamsEvent.ExitTestExam)
            }
        }
    }


    private fun showLoading() {
        state = state.copy(isLoading = true)
    }

    private fun hideLoading() {
        state = state.copy(isLoading = false)
    }

    private fun showError() {
        state = state.copy(isError = true)
    }

    private fun hideError() {
        state = state.copy(isError = false)
    }
}