package com.br.alfabetizamais.features.enter.use_case

import com.br.alfabetizamais.features.enter.domain.model.ValidationResult

class ValidateUser {

    fun execute(user: String): ValidationResult {
        if (user.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "O Usuario não pode ser em branco"
            )
        }

        return ValidationResult(
            successful = true,
            errorMessage = null
        )
    }
}