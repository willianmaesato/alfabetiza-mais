package com.br.alfabetizamais.features.profile.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GradeAverageTestExamResponse (
    val nota: Double,

    @SerialName("data_finalizacao")
    val dataFinalizacao: String,

    val titulo: String,
    val media: Long,

    @SerialName("valor_prova")
    val valorProva: Long,

    val resultado: String
)
