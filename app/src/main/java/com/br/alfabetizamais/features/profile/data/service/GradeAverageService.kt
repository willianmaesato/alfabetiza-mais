package com.br.alfabetizamais.features.profile.data.service

import com.br.alfabetizamais.features.profile.data.model.GradeAverageTestExamResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface GradeAverageService {

    @GET("nota/notaaluno/{id}")
    suspend fun getListGradeAverage(@Path("id") id: Int): List<GradeAverageTestExamResponse>
}