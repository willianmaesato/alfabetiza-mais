package com.br.alfabetizamais.features.home.domain.repository

import com.br.alfabetizamais.features.home.data.model.AvailableTestExamsResponse
import com.br.alfabetizamais.features.home.domain.model.AvailableTestExamsData
import com.br.alfabetizamais.features.home.domain.model.TestExamData
import kotlinx.coroutines.flow.Flow

interface AvailableTestExamsRepository {
    fun listAvailableTestExams(idTurma: Int) : Flow<List<AvailableTestExamsData>>
}