package com.br.alfabetizamais.features.home.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class AvailableTestExamsResponse(
    val prova: TestExamResponse
)

@Serializable
data class TestExamResponse(
    val id: Int,
    val titulo: String,
    val media: Int,

    @SerialName("valor_prova")
    val valorProva: Int,

    @SerialName("data_liberacao")
    val dataLiberacao: String,

    @SerialName("date_encerramento")
    val dateEncerramento: String,

    @SerialName("quantidade_tentativas")
    val quantidadeTentativas: Int
)
