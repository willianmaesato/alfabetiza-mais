package com.br.alfabetizamais.features.profile.use_case

import com.br.alfabetizamais.features.profile.domain.model.GradeAverageTestExamData
import com.br.alfabetizamais.features.profile.domain.repository.GradeAverageRepository
import kotlinx.coroutines.flow.Flow

class GradeAverageUseCase(private val gradeAverageRepository: GradeAverageRepository) {
    suspend fun invoke(idAluno: Int): Flow<List<GradeAverageTestExamData>> =
        gradeAverageRepository.getListGradeAverage(idAluno)
}