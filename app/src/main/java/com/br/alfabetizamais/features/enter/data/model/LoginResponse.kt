package com.br.alfabetizamais.features.enter.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class LoginResponse(
    @SerialName("usuario")
    val user: String,

    @SerialName("senha")
    val password: String
)