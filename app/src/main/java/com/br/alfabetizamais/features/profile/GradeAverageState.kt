package com.br.alfabetizamais.features.profile

import com.br.alfabetizamais.features.profile.domain.model.GradeAverageTestExamData

data class GradeAverageState(
    val isLoading: Boolean = true,
    val list: List<GradeAverageTestExamData> = emptyList()
)