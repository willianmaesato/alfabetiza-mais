package com.br.alfabetizamais.features.enter.presenter

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.br.alfabetizamais.R
import com.br.alfabetizamais.features.components.RoundedButton
import com.br.alfabetizamais.navigation.Screen
import com.br.alfabetizamais.ui.theme.LightPurple
import com.br.alfabetizamais.ui.theme.MediumPurple
import com.google.accompanist.systemuicontroller.rememberSystemUiController

@Composable
fun EnterScreen(navController: NavController) {

    val systemUiController = rememberSystemUiController()
    SideEffect {
        systemUiController.setSystemBarsColor(
            color = MediumPurple
        )
    }

    Scaffold(backgroundColor = MediumPurple) {
        Column(
            Modifier
                .fillMaxSize()
                .padding(bottom = 20.dp, top = 20.dp),
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "Alfabetiza +",
                fontSize = 36.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(horizontal = 20.dp),
                color = Color.White,
                fontWeight = FontWeight.Bold
            )
            Spacer(modifier = Modifier.padding(top = 20.dp))
            Text(
                text = "Aplicativo voltado para auxilio no desenvolvimento escolar de crianças de ensino fundamental!",
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(horizontal = 20.dp),
                color = Color.White,
                fontWeight = FontWeight.Bold
            )
            Spacer(modifier = Modifier.padding(top = 40.dp))
            Box(
                contentAlignment = Alignment.Center
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_enter),
                    contentDescription = "Logo",
                    modifier = Modifier.scale(1f)
                )
            }
            Spacer(modifier = Modifier.padding(top = 60.dp))
            Column(modifier = Modifier.padding(horizontal = 20.dp)) {
                RoundedButton(
                    text = "Entrar",
                    onClick = {
                        navController.navigate(Screen.LoginScreen.route)
                    },
                    displayProgressBar = false,
                    buttonElevation = ButtonDefaults.elevation(
                        defaultElevation = 0.dp,
                        disabledElevation = 0.dp,
                        pressedElevation = 0.dp
                    ),
                    buttonColors = ButtonDefaults.buttonColors(
                        backgroundColor = Color.White,
                        contentColor = LightPurple
                    ),
                    colorText = Color.Black
                )
                Spacer(modifier = Modifier.padding(4.dp))
            }
        }
    }
}