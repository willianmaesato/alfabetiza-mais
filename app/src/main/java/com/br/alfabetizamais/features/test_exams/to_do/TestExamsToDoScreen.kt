package com.br.alfabetizamais.features.test_exams.to_do

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.*
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.annotation.ExperimentalCoilApi
import coil.compose.ImagePainter
import coil.compose.rememberImagePainter
import com.airbnb.lottie.compose.*
import com.br.alfabetizamais.R
import com.br.alfabetizamais.domain.model.TestExamData
import com.br.alfabetizamais.features.components.*
import com.br.alfabetizamais.ui.theme.LightGray
import com.br.alfabetizamais.ui.theme.MediumPurple
import kotlinx.coroutines.flow.collect
import java.util.*

@OptIn(ExperimentalFoundationApi::class, ExperimentalCoilApi::class)
@Composable
fun TestExamsToDoScreen(
    navController: NavController,
    viewModel: TestExamsToDoViewModel = hiltViewModel(),
    testExam: TestExamData,
    idAluno: Int
) {

    val state = viewModel.state

    viewModel.setTestExam(testExam, idAluno)

    val openExitDialog = remember { mutableStateOf(false) }

    val context = LocalContext.current
    LaunchedEffect(key1 = context) {
        viewModel.validationEvents.collect { event ->
            when (event) {
                TestExamsEvent.ExitTestExam -> {
                    openExitDialog.value = true
                }
                TestExamsEvent.ExitWithErrorTestExam -> {
                    navController.popBackStack()
                }
                TestExamsEvent.FinishTestExam -> {
                    navController.popBackStack()
                }
            }
        }
    }

    BackPressHandler(onBackPressed = {
        viewModel.exit()
    })

    Scaffold(
        backgroundColor = LightGray,
        topBar = {
            CustomTopAppBar(
                viewModel.getTitleTestExam(),
                isEnableTextAux = true,
                textAux = viewModel.getStepByStep()
            )
        }, content = {
            Card(
                backgroundColor = Color.White,
                shape = RoundedCornerShape(topEnd = 40.dp, topStart = 40.dp),
                modifier = Modifier.fillMaxSize(),
                elevation = 0.dp
            ) {
                if (!state.isError) {
                    if (!state.isLoading) {
                        if (state.gradeTestExamData != null) {
                            Column(
                                horizontalAlignment = Alignment.CenterHorizontally,
                                verticalArrangement = Arrangement.Top
                            ) {
                                val list =
                                    when {
                                        state.gradeTestExamData.nota < state.gradeTestExamData.mediaProva -> listOf(
                                            R.raw.sad_emoji,
                                            "Não foi desta vez, mas você pode melhorar!"
                                        )
                                        state.gradeTestExamData.mediaProva == state.gradeTestExamData.nota -> listOf(
                                            R.raw.sad_inexpressive_no_expressions,
                                            "Parabêns você foi na bem, continue assim!"
                                        )
                                        else -> listOf(
                                            R.raw.happy_emoji,
                                            "Meus parabeêns você foi muito bem!"
                                        )
                                    }

                                if (state.gradeTestExamData.exercisethree == 0) {
                                    val compositionResult: LottieCompositionResult =
                                        rememberLottieComposition(LottieCompositionSpec.RawRes(list[0] as Int))

                                    val progress by animateLottieCompositionAsState(
                                        compositionResult.value,
                                        isPlaying = true,
                                        iterations = LottieConstants.IterateForever,
                                        speed = 1.0f
                                    )
                                    LottieAnimation(
                                        compositionResult.value,
                                        progress,
                                        Modifier
                                            .fillMaxWidth()
                                            .height(300.dp)
                                    )
                                    Text(
                                        modifier = Modifier
                                            .align(Alignment.CenterHorizontally)
                                            .padding(vertical = 10.dp),
                                        text = list[1].toString(),
                                        fontWeight = FontWeight.Bold,
                                        textAlign = TextAlign.Center,
                                        fontSize = 16.sp,
                                        color = Color.Black
                                    )
                                } else {
                                    Image(
                                        painter = painterResource(id = R.drawable.ic_math_book),
                                        contentDescription = null,
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .height(300.dp)
                                    )
                                    Text(
                                        modifier = Modifier
                                            .align(Alignment.CenterHorizontally)
                                            .padding(vertical = 10.dp),
                                        text = "Aguarde a prova ser corrigida\npelor professor responsavel",
                                        fontWeight = FontWeight.Bold,
                                        textAlign = TextAlign.Center,
                                        fontSize = 16.sp,
                                        color = Color.Black
                                    )
                                }
                            }
                        } else {
                            Column(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .background(Color.White)
                                    .padding(horizontal = 20.dp),
                                horizontalAlignment = Alignment.Start,
                                verticalArrangement = Arrangement.Top
                            ) {
                                if (state.exerciseByStep != null) {
                                    Text(
                                        modifier = Modifier
                                            .align(Alignment.CenterHorizontally)
                                            .padding(vertical = 10.dp),
                                        text = state.exerciseByStep.textoAuxiliar!!,
                                        fontWeight = FontWeight.Bold,
                                        textAlign = TextAlign.Center,
                                        fontSize = 18.sp
                                    )
                                    when (state.exerciseByStep.tipoExercicio) {
                                        1 -> {
                                            CoilImage(state.exerciseByStep.imagemEnunciado!!)
                                            Text(
                                                modifier = Modifier
                                                    .align(Alignment.CenterHorizontally)
                                                    .padding(vertical = 10.dp),
                                                text = state.exerciseByStep.enunciado!!,
                                                fontWeight = FontWeight.Bold,
                                                textAlign = TextAlign.Center,
                                                fontSize = 16.sp
                                            )
                                            LazyColumn(content = {
                                                items(state.firstQuestionType) { item ->
                                                    LabelledCheckBox(item.selected, {
                                                        viewModel.setAnswersForFirstType(it, item)
                                                    }, label = item.title)
                                                }
                                            })
                                        }
                                        2 -> {
                                            CoilImage(state.exerciseByStep.imagemEnunciado!!)
                                            Text(
                                                modifier = Modifier
                                                    .align(Alignment.CenterHorizontally)
                                                    .padding(vertical = 10.dp),
                                                text = state.exerciseByStep.enunciado!!,
                                                fontWeight = FontWeight.Bold,
                                                textAlign = TextAlign.Center,
                                                fontSize = 16.sp
                                            )
                                            LazyColumn(content = {
                                                items(state.secondQuestionType) { item ->
                                                    LabelledCheckBox(item.selected, {
                                                        viewModel.setAnswersForSecondType(it, item)
                                                    }, label = item.title)
                                                }
                                            })
                                        }
                                        3 -> {
                                            CoilImage(state.exerciseByStep.imagemEnunciado!!)
                                            Text(
                                                modifier = Modifier
                                                    .align(Alignment.CenterHorizontally)
                                                    .padding(vertical = 10.dp),
                                                text = state.exerciseByStep.enunciado!!,
                                                fontWeight = FontWeight.Bold,
                                                textAlign = TextAlign.Center,
                                                fontSize = 16.sp
                                            )
                                            StandardTextField(
                                                text = state.thirdQuestionType,
                                                hint = "Discursiva",
                                                onValueChange = {
                                                    viewModel.setAnswersForThirdType(it)
                                                },
                                                keyboardType = KeyboardType.Text,
                                                maxLines = 10,
                                                keyBoardActions = KeyboardActions(
                                                    onDone = {
                                                        viewModel.nextTestExams()
                                                    }
                                                )
                                            )
                                        }
                                        4 -> {
                                            Text(
                                                modifier = Modifier
                                                    .align(Alignment.CenterHorizontally)
                                                    .padding(vertical = 10.dp),
                                                text = state.exerciseByStep.enunciado!!,
                                                fontWeight = FontWeight.Bold,
                                                textAlign = TextAlign.Center,
                                                fontSize = 16.sp
                                            )
                                            Text(
                                                modifier = Modifier
                                                    .align(Alignment.CenterHorizontally)
                                                    .padding(vertical = 10.dp),
                                                text = state.fourthQuestionType.mathDescription,
                                                fontWeight = FontWeight.Bold,
                                                textAlign = TextAlign.Center,
                                                fontSize = 22.sp
                                            )
                                            StandardTextField(
                                                text = state.fourthQuestionType.answerToMath,
                                                hint = "Resolução",
                                                onValueChange = {
                                                    viewModel.setAnswersForFourthType(it)
                                                },
                                                keyboardType = KeyboardType.Number,
                                                maxLines = 1,
                                                singleLine = true,
                                                keyBoardActions = KeyboardActions(
                                                    onDone = {
                                                        viewModel.nextTestExams()
                                                    }
                                                )
                                            )
                                        }
                                        5 -> {
                                            CoilImage(state.exerciseByStep.imagemEnunciado!!)
                                            Text(
                                                modifier = Modifier
                                                    .align(Alignment.CenterHorizontally)
                                                    .padding(vertical = 10.dp),
                                                text = state.exerciseByStep.enunciado!!,
                                                fontWeight = FontWeight.Bold,
                                                textAlign = TextAlign.Center,
                                                fontSize = 16.sp
                                            )
                                            LazyVerticalGrid(
                                                cells = GridCells.Fixed(6),
                                                contentPadding = PaddingValues(10.dp),
                                            ) {
                                                items(state.fifthQuestionType) {
                                                    TagHost(
                                                        isSelected = it.selected,
                                                        text = it.title,
                                                        onClick = {
                                                            viewModel.setAnswersForFifthType(it)
                                                        }
                                                    )
                                                }
                                            }
                                        }
                                        6 -> {
                                            Text(
                                                modifier = Modifier
                                                    .align(Alignment.CenterHorizontally)
                                                    .padding(vertical = 10.dp),
                                                text = state.exerciseByStep.enunciado!!,
                                                fontWeight = FontWeight.Bold,
                                                textAlign = TextAlign.Center,
                                                fontSize = 16.sp
                                            )
                                            LazyVerticalGrid(
                                                cells = GridCells.Fixed(2),
                                                contentPadding = PaddingValues(10.dp),
                                            ) {
                                                items(state.sixthQuestionType) { item ->
                                                    Column(modifier = Modifier.padding(10.dp)) {
                                                        CoilImage(item.path)
                                                        StandardTextField(
                                                            text = item.answerAsked,
                                                            hint = "Resposta",
                                                            onValueChange = {
                                                                viewModel.setAnswersForSixthType(
                                                                    it.uppercase(Locale.getDefault()),
                                                                    item
                                                                )
                                                            },
                                                            keyboardType = KeyboardType.Text,
                                                            maxLines = 1,
                                                            singleLine = true
                                                        )
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            CircularProgressIndicator()
                        }
                    }
                } else {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_error),
                            contentDescription = null,
                            modifier = Modifier.size(76.dp)
                        )
                        Text(
                            text = "Erro ao carregar informações",
                            modifier = Modifier.padding(top = 40.dp)
                        )
                    }
                }
            }
        }, bottomBar = {
            if (!state.isLoading) {
                RoundedButton(
                    modifier = Modifier.padding(20.dp),
                    text = viewModel.getTextButton(),
                    onClick = { viewModel.nextTestExams() },
                    isEnable = viewModel.isEnableButton(),
                    displayProgressBar = state.isShowProgressBar
                )
            }
        })

    if (openExitDialog.value) {
        Dialog(onDismissRequest = { openExitDialog.value = false }) {
            CustomDialogUI(
                openDialogCustom = openExitDialog,
                title = "Deseja realmente sair da prova?\nPois não irá poder fazer ela novamente...",
                fontSizeTitle = 12.sp,
                onClickYes = {
                    navController.popBackStack()
                })
        }
    }
}

@Composable
fun LabelledCheckBox(
    checked: Boolean,
    onCheckedChange: ((Boolean) -> Unit),
    label: String,
    modifier: Modifier = Modifier
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .clip(MaterialTheme.shapes.small)
            .clickable(
                indication = rememberRipple(color = MaterialTheme.colors.primary),
                interactionSource = remember { MutableInteractionSource() },
                onClick = { onCheckedChange(!checked) }
            )
            .requiredHeight(ButtonDefaults.MinHeight)
            .padding(4.dp)
    ) {
        Checkbox(
            checked = checked,
            onCheckedChange = null,
            colors = CheckboxDefaults.colors(MediumPurple)
        )
        Spacer(Modifier.size(6.dp))
        Text(
            text = label,
        )
    }
}

@ExperimentalCoilApi
@Composable
fun CoilImage(urlImage: String) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(120.dp),
        contentAlignment = Alignment.Center
    ) {
        val painter = rememberImagePainter(
            data = urlImage,
            builder = {
                crossfade(true)
                error(R.drawable.ic_baseline_error_outline_24)
            }
        )

        val state = painter.state
        if (state is ImagePainter.State.Loading) {
            CircularProgressIndicator()
        }

        Image(
            painter = painter,
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .height(120.dp)
                .align(Alignment.Center)
        )
    }
}