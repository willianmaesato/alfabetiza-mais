package com.br.alfabetizamais.features.home

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.text.toLowerCase
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.br.alfabetizamais.R
import com.br.alfabetizamais.data.PreferencesManager
import com.br.alfabetizamais.domain.model.TestExamData
import com.br.alfabetizamais.features.home.domain.model.AvailableTestExamsData
import com.br.alfabetizamais.features.home.use_case.AvailableTestExamsUseCase
import com.br.alfabetizamais.ui.theme.*
import com.br.alfabetizamais.util.formatDate
import com.br.alfabetizamais.util.unaccent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@RequiresApi(Build.VERSION_CODES.O)
@HiltViewModel
class HomeViewModel @Inject constructor(
    private val preferencesManager: PreferencesManager,
    private val availableTestExamsUseCase: AvailableTestExamsUseCase
) : ViewModel() {

    var state by mutableStateOf(HomeState())

    private var listTestExams: List<TestExamData> = emptyList()

    private val validationEventChannel = Channel<HomeEvent>()
    val validationEvents = validationEventChannel.receiveAsFlow()

    init {
        getUserLogged()
    }

    private fun getUserLogged() {
        viewModelScope.launch {
            preferencesManager.userDataInit
                .flowOn(Dispatchers.IO)
                .collect { user ->
                    state = state.copy(userName = user.usuario, idAluno = user.id)
                    getListTestExam(user.idTurma)
                }
        }
    }

    private fun getListTestExam(idTurma: Int) {
        viewModelScope.launch {
            availableTestExamsUseCase.invoke(idTurma)
                .flowOn(Dispatchers.IO)
                .onStart { showLoading() }
                .catch { hideLoading() }
                .onCompletion { }
                .collect(::handleTestExam)
        }
    }

    private fun handleTestExam(availableTestExams: List<AvailableTestExamsData>) {
        val listAux: ArrayList<TestExamData> = arrayListOf()
        availableTestExams.forEach { testExam ->
            val isMath = testExam.prova.titulo.lowercase().contains("mat")
            listAux.add(
                TestExamData(
                    idProva = testExam.prova.id,
                    mediaProva = testExam.prova.media,
                    valorProva = testExam.prova.valorProva,
                    titleSchoolSubject = testExam.prova.titulo,
                    exerciseSchoolSubjectTitle = "Disponível\nde: ${formatDate(testExam.prova.dataLiberacao)}\naté: ${formatDate(testExam.prova.dateEncerramento)}",
                    image = if (isMath) R.drawable.ic_math_book else R.drawable.ic_literature_book,
                    lightColor = if (isMath) Blue100 else Red100,
                    mediumColor = if (isMath) Blue200 else Red200,
                    highColor = if (isMath) Blue300 else Red300
                )
            )
        }
        listTestExams = listAux
        state = state.copy(
            list = listAux,
            isLoading = false
        )
    }

    fun searchTestExam(search: String) {
        state = if (search.isEmpty()) {
            state.copy(list = listTestExams, search = search)
        } else {
            state.copy(list = filterListBySearch(search.lowercase(Locale.getDefault())), search = search)
        }
    }

    fun logoutApp() {
        viewModelScope.launch {
            preferencesManager.logoutUser()
            validationEventChannel.send(HomeEvent.Logout)
        }
    }

    private fun filterListBySearch(search: String): List<TestExamData> {
        return listTestExams.filter { item ->
            item.titleSchoolSubject
                .lowercase()
                .unaccent()
                .contains(search) ||
                    item.exerciseSchoolSubjectTitle
                        .lowercase()
                        .unaccent()
                        .contains(search)
        }
    }

    private fun showLoading() {
        state = state.copy(isLoading = true)
    }

    private fun hideLoading() {
        state = state.copy(isLoading = false)
    }
}