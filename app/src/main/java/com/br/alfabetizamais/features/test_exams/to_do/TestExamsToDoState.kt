package com.br.alfabetizamais.features.test_exams.to_do

import com.br.alfabetizamais.domain.model.ExercicioData
import com.br.alfabetizamais.domain.model.GradeTestExamData
import com.br.alfabetizamais.domain.model.TestExamsData
import com.br.alfabetizamais.features.test_exams.domain.*

data class TestExamsToDoState(
    val isLoading: Boolean = false,
    val isError: Boolean = true,
    val testExam: List<TestExamsData> = emptyList(),
    val stepTestExams: Int = 0,
    val isLastQuestion: Boolean = false,
    val exerciseByStep: ExercicioData? = null,
    val answers: TestAnswersData? = null,
    val firstQuestionType: List<MultipleWithOneAnswer> = emptyList(),
    val secondQuestionType: List<MultipleWithOneAnswer> = emptyList(),
    val thirdQuestionType: String = "",
    val fourthQuestionType: MathAnswer = MathAnswer("", ""),
    val fifthQuestionType: List<MultipleWithOneAnswer> = emptyList(),
    val sixthQuestionType: List<PathAnswer> = emptyList(),
    val isEnableButton: Boolean = false,
    val isShowProgressBar: Boolean = false,
    val gradeTestExamData: GradeTestExamData? = null
)