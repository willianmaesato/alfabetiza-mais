package com.br.alfabetizamais.features.test_exams.domain.repository

import com.br.alfabetizamais.data.model.NotaResponse
import com.br.alfabetizamais.domain.model.GradeTestExamData
import com.br.alfabetizamais.domain.model.TestExamsData
import com.br.alfabetizamais.features.home.domain.model.AvailableTestExamsData
import kotlinx.coroutines.flow.Flow

interface TestExamsToDoRepository {
    fun getListTestExams(idProva: Int): Flow<List<TestExamsData>>
    fun sendGradeTestExam(gradeTestExam: GradeTestExamData): Flow<NotaResponse>
}