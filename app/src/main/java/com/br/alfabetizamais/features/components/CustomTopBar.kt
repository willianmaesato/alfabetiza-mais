package com.br.alfabetizamais.features.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Logout
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.br.alfabetizamais.ui.theme.LightGray
import com.br.alfabetizamais.ui.theme.MediumPurple

@Composable
fun CustomTopAppBar(
    title: String,
    textAux: String = "",
    isEnableLogoutButton: Boolean = false,
    isEnableTextAux: Boolean = false,
    onClickLogoutButton: () -> (Unit) = {}
) {
    TopAppBar(
        elevation = 0.dp,
        modifier = Modifier.fillMaxWidth(),
        title = {
            Box(modifier = Modifier.fillMaxWidth()) {
                Text(
                    text = title,
                    modifier = Modifier.align(Alignment.Center),
                    color = Color.Black,
                    style = MaterialTheme.typography.body1,
                    fontSize = 22.sp,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
                if (isEnableLogoutButton) {
                    IconButton(
                        onClick = { onClickLogoutButton() }, modifier = Modifier
                            .align(Alignment.CenterEnd)
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Logout,
                            contentDescription = "Navigate back",
                            tint = Color.Black
                        )
                    }
                }
                if(isEnableTextAux){
                    Box(modifier = Modifier
                            .align(Alignment.CenterStart)
                    ) {
                        Text(
                            text = textAux,
                            modifier = Modifier.align(Alignment.Center),
                            color = Color.Black,
                            style = MaterialTheme.typography.body1,
                            fontSize = 22.sp,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis
                        )
                    }
                }
            }
        },
        backgroundColor = LightGray,
    )
}