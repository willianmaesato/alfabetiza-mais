package com.br.alfabetizamais.features.enter.use_case

import com.br.alfabetizamais.domain.model.UserData
import com.br.alfabetizamais.features.enter.domain.model.LoginData
import com.br.alfabetizamais.features.enter.domain.repositorry.LoginRepository
import kotlinx.coroutines.flow.Flow

class LoginUseCase(private val loginRepository: LoginRepository) {
    suspend fun invoke(login: LoginData): Flow<UserData> =
        loginRepository.login(login)
}