package com.br.alfabetizamais.features.test_exams.domain

data class TestAnswersData(
    val idProva: Int,
    val idAluno: Int,
    val exercicios: List<AnswersData>
)

data class AnswersData(
    val idExercicio: Int,
    val resposta: String,
    val status: Boolean,
    val valorQuestao: Int,
    val questao: List<AskData>? = null
)

data class AskData(
    val status: Boolean,
    val resposta: String
)