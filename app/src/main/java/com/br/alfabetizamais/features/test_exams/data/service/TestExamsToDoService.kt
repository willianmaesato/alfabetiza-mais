package com.br.alfabetizamais.features.test_exams.data.service

import com.br.alfabetizamais.data.model.GradeTestExamResponse
import com.br.alfabetizamais.data.model.NotaResponse
import com.br.alfabetizamais.data.model.TestExamsResponse
import com.br.alfabetizamais.features.home.data.model.AvailableTestExamsResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface TestExamsToDoService {

    @GET("prova/exerciciosporprova/{id_prova}")
    suspend fun getListTestExams(@Path("id_prova") idProva: Int): List<TestExamsResponse>

    @POST("nota")
    suspend fun sendGradeTestExam(@Body idProva: GradeTestExamResponse): NotaResponse
}