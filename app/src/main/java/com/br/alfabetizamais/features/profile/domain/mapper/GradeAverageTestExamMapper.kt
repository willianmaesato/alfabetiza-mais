package com.br.alfabetizamais.features.profile.domain.mapper

import com.br.alfabetizamais.features.profile.data.model.GradeAverageTestExamResponse
import com.br.alfabetizamais.features.profile.domain.model.GradeAverage
import com.br.alfabetizamais.features.profile.domain.model.GradeAverageTestExamData

fun GradeAverageTestExamResponse.toDomain(): GradeAverageTestExamData {
    return GradeAverageTestExamData(
        nota = nota,
        media = media,
        titulo = titulo,
        dataFinalizacao = dataFinalizacao,
        resultado = GradeAverage.getDocumentType(resultado),
        valorProva = valorProva
    )
}