package com.br.alfabetizamais.features.enter.domain.model

data class ValidationResult(
    val successful: Boolean,
    val errorMessage: String? = null
)