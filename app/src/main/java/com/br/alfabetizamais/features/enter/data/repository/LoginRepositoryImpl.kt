package com.br.alfabetizamais.features.enter.data.repository

import com.br.alfabetizamais.domain.mapper.toDomain
import com.br.alfabetizamais.domain.model.UserData
import com.br.alfabetizamais.features.enter.data.service.LoginService
import com.br.alfabetizamais.features.enter.domain.mapper.toRemote
import com.br.alfabetizamais.features.enter.domain.model.LoginData
import com.br.alfabetizamais.features.enter.domain.repositorry.LoginRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class LoginRepositoryImpl(private val service: LoginService) : LoginRepository {
    override fun login(login: LoginData): Flow<UserData> = flow {
        emit(service.login(login.toRemote()).user.first().toDomain())
    }
}