package com.br.alfabetizamais.features.test_exams.to_do

sealed class TestExamsEvent {
    object FinishTestExam : TestExamsEvent()
    object ExitTestExam : TestExamsEvent()
    object ExitWithErrorTestExam : TestExamsEvent()
}