package com.br.alfabetizamais.features.home.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class AvailableTestExamsData(
    val prova: TestExamData
)

@Serializable
data class TestExamData (
    val id: Int,
    val titulo: String,
    val media: Int,
    val valorProva: Int,
    val dataLiberacao: String,
    val dateEncerramento: String,
    val quantidadeTentativas: Int
)