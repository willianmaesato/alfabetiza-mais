package com.br.alfabetizamais.features.enter.presenter.login

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.br.alfabetizamais.data.PreferencesManager
import com.br.alfabetizamais.features.enter.domain.model.LoginData
import com.br.alfabetizamais.features.enter.use_case.LoginUseCase
import com.br.alfabetizamais.features.enter.use_case.ValidatePassword
import com.br.alfabetizamais.features.enter.use_case.ValidateUser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val preferencesManager: PreferencesManager,
    private val validateUser: ValidateUser,
    private val validatePassword: ValidatePassword,
    private val loginUseCase: LoginUseCase
) : ViewModel() {

    var state by mutableStateOf(LoginState())

    private val validationEventChannel = Channel<LoginEvent>()
    val validationEvents = validationEventChannel.receiveAsFlow()

    fun changeEmail(user: String) {
        state = state.copy(user = user)
    }

    fun changePassword(password: String) {
        state = state.copy(password = password)
    }

    fun submitData() {
        if (state.isLoading) {
            return
        }
        val userResult = validateUser.execute(state.user)
        val passwordResult = validatePassword.execute(state.password)
        val hasError = listOf(
            userResult,
            passwordResult
        ).any { !it.successful }

        state = state.copy(
            userError = userResult.errorMessage,
            passwordError = passwordResult.errorMessage,
        )

        if (hasError) {
            return
        }

        viewModelScope.launch {
            loginUseCase.invoke(LoginData(user = state.user, password = state.password))
                .flowOn(Dispatchers.IO)
                .onStart { showLoading() }
                .catch {
                    handlerError()
                }
                .onCompletion { hideLoading() }
                .collect { user ->
                    preferencesManager.updateUserName(user)
                    validationEventChannel.send(LoginEvent.Success)
                }
        }
    }

    private fun handlerError() {
        hideLoading()
        viewModelScope.launch {
            validationEventChannel.send(LoginEvent.Error)
        }
    }

    private fun showLoading() {
        state = state.copy(isLoading = true)
    }

    private fun hideLoading() {
        state = state.copy(isLoading = false)
    }
}