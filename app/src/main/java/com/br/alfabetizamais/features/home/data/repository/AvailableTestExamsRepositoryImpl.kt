package com.br.alfabetizamais.features.home.data.repository

import com.br.alfabetizamais.features.home.data.service.AvailableTestExamsService
import com.br.alfabetizamais.features.home.domain.mapper.toDomain
import com.br.alfabetizamais.features.home.domain.model.AvailableTestExamsData
import com.br.alfabetizamais.features.home.domain.model.TestExamData
import com.br.alfabetizamais.features.home.domain.repository.AvailableTestExamsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class AvailableTestExamsRepositoryImpl(private val availableTestExamsService: AvailableTestExamsService) : AvailableTestExamsRepository {
    override fun listAvailableTestExams(idTurma: Int): Flow<List<AvailableTestExamsData>> = flow {
        emit(availableTestExamsService.getAvailableTestExams(idTurma).map { it.toDomain() })
    }
}