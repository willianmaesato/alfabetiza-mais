package com.br.alfabetizamais.features.test_exams.domain

data class MultipleWithOneAnswer(
    val title: String,
    var selected: Boolean,
    val answer: String
)

data class MathAnswer(
    val mathDescription: String,
    var answerToMath: String
)

data class PathAnswer(
    val id: Int,
    val idExercise: Int,
    val path: String,
    val answer: String,
    var answerAsked: String
)