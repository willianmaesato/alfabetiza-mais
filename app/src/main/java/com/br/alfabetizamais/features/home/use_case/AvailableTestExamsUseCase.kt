package com.br.alfabetizamais.features.home.use_case

import com.br.alfabetizamais.features.home.domain.model.AvailableTestExamsData
import com.br.alfabetizamais.features.home.domain.model.TestExamData
import com.br.alfabetizamais.features.home.domain.repository.AvailableTestExamsRepository
import kotlinx.coroutines.flow.Flow

class AvailableTestExamsUseCase(private val availableTestExamsRepository: AvailableTestExamsRepository) {

    suspend fun invoke(idTurma: Int): Flow<List<AvailableTestExamsData>> =
        availableTestExamsRepository.listAvailableTestExams(idTurma)

}