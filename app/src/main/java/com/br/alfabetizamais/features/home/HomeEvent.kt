package com.br.alfabetizamais.features.home

sealed class HomeEvent {
    object Logout : HomeEvent()
}