package com.br.alfabetizamais.features.test_exams.data.repository

import com.br.alfabetizamais.data.model.NotaResponse
import com.br.alfabetizamais.domain.mapper.toDomain
import com.br.alfabetizamais.domain.mapper.toRemote
import com.br.alfabetizamais.domain.model.GradeTestExamData
import com.br.alfabetizamais.domain.model.TestExamsData
import com.br.alfabetizamais.features.home.domain.mapper.toDomain
import com.br.alfabetizamais.features.home.domain.model.AvailableTestExamsData
import com.br.alfabetizamais.features.test_exams.data.service.TestExamsToDoService
import com.br.alfabetizamais.features.test_exams.domain.repository.TestExamsToDoRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class TestExamsToDoToDoRepositoryImpl(private val service: TestExamsToDoService) :
    TestExamsToDoRepository {
    override fun getListTestExams(idProva: Int): Flow<List<TestExamsData>> = flow {
        emit(service.getListTestExams(idProva).map { it.toDomain() })
    }

    override fun sendGradeTestExam(gradeTestExam: GradeTestExamData): Flow<NotaResponse> =
        flow {
            emit(service.sendGradeTestExam(gradeTestExam.toRemote()))
        }
}