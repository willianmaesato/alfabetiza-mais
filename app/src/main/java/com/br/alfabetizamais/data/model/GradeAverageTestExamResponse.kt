package com.br.alfabetizamais.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GradeAverageTestExamResponse (
    @SerialName("id_aluno")
    val idAluno: Int,

    @SerialName("id_prova")
    val idProva: Int,

    val nota: Double,
    val exercisethree: Int
)
