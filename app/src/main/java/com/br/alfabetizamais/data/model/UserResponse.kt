package com.br.alfabetizamais.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UserResponse(
    val id: Int,
    @SerialName("id_turma")
    val idTurma: Int,
    val nome: String,
    val usuario: String,
    val senha: String,
    val idade: Int = 0,
    @SerialName("nome_completo_responsavel")
    val nomeCompletoResponsavel: String,
    @SerialName("email_responsavel")
    val emailResponsavel: String
)

