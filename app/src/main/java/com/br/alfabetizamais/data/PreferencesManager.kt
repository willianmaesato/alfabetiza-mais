package com.br.alfabetizamais.data

import android.content.Context
import androidx.datastore.createDataStore
import com.br.alfabetizamais.domain.model.UserData
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesManager @Inject constructor(@ApplicationContext context: Context) {

    private val dataStore = context.createDataStore("MarketPreferences", UserSerializer)

    val isLogged = dataStore.data
        .map { preferences ->
            return@map (preferences.emailResponsavel.isNotEmpty()
                    && preferences.usuario.isNotEmpty()
                    && preferences.idade != -1
                    && preferences.nome.isNotEmpty()
                    && preferences.senha.isNotEmpty()
                    && preferences.idTurma != -1)

        }.catch { exception ->
            if (exception is IOException) {
                Timber.e(exception, "Error reading preferences")
                emit(false)
            } else {
                throw exception
            }
        }

    val userDataInit = dataStore.data
        .map { preferences ->
            preferences.copy(
                emailResponsavel = preferences.emailResponsavel,
                idade = preferences.idade,
                idTurma = preferences.idTurma,
                nome = preferences.nome,
                nomeCompletoResponsavel = preferences.nomeCompletoResponsavel,
                senha = preferences.senha,
                usuario = preferences.usuario
            )
        }.catch { exception ->
            if (exception is IOException) {
                Timber.e(exception, "Error reading preferences")
                emit(UserData())
            } else {
                throw exception
            }
        }

    suspend fun updateUserName(userDataToSet: UserData) {
        dataStore.updateData { userData ->
            userData.copy(
                id = userDataToSet.id,
                idTurma = userDataToSet.idTurma,
                nome = userDataToSet.nome,
                usuario = userDataToSet.usuario,
                senha = userDataToSet.senha,
                idade = userDataToSet.idade,
                nomeCompletoResponsavel = userDataToSet.nomeCompletoResponsavel,
                emailResponsavel = userDataToSet.emailResponsavel
            )
        }
    }

    suspend fun logoutUser() {
        dataStore.updateData { userData ->
            userData.copy(
                emailResponsavel = "",
                idade = -1,
                idTurma = -1,
                nome = "",
                nomeCompletoResponsavel = "",
                senha = "",
                usuario = ""
            )
        }
    }
}