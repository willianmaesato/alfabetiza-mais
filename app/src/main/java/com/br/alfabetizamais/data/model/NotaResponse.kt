package com.br.alfabetizamais.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NotaResponse (
    val success: Boolean,
    val message: String,
    val data: DataResponse
)

@Serializable
data class DataResponse (
    @SerialName("id_aluno")
    val idAluno: Long,

    @SerialName("id_prova")
    val idProva: Long,

    val nota: Long,

    @SerialName("data_finalizacao")
    val dataFinalizacao: String,

    val exercisethree: Long,
    val id: Long
)
