package com.br.alfabetizamais.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TestExamsResponse(
    val prova: ProvaResponse
)

@Serializable
data class ProvaResponse(
    val id: Int,

    @SerialName("id_prova")
    val idProva: Int,

    @SerialName("id_exercicio")
    val idExercicio: Int,

    val ordem: Int,

    @SerialName("valor_exercicio")
    val valorExercicio: Int,

    val exercicio: ExercicioResponse
)

@Serializable
data class ExercicioResponse(
    val id: Int? = null,
    val name: String? = null,
    val materia: String? = null,

    @SerialName("tipo_exercicio")
    val tipoExercicio: Int? = null,

    @SerialName("texto_auxiliar")
    val textoAuxiliar: String? = null,

    @SerialName("imagem_enunciado")
    val imagemEnunciado: String? = null,

    val enunciado: String? = null,

    @SerialName("enunciado_questao_a")
    val enunciadoQuestaoA: String? = null,

    @SerialName("enunciado_questao_b")
    val enunciadoQuestaoB: String? = null,

    @SerialName("enunciado_questao_c")
    val enunciadoQuestaoC: String? = null,

    @SerialName("enunciado_questao_d")
    val enunciadoQuestaoD: String? = null,

    @SerialName("enunciado_questao_e")
    val enunciadoQuestaoE: String? = null,

    @SerialName("resposta_questao")
    val respostaQuestao: String? = null,

    @SerialName("numero_a")
    val numeroA: Int? = null,

    @SerialName("numero_b")
    val numeroB: Int? = null,

    val operacao: String? = null,

    @SerialName("numero_minimo")
    val numeroMinimo: Int? = null,

    @SerialName("numero_maximo")
    val numeroMaximo: Int? = null,

    val exercicioTipoSeis: Map<String, QuestaoPathResponse>? = null
)

@Serializable
data class QuestaoPathResponse(
    val id: Int,
    @SerialName("id_exercicio")
    val idExercicio: Int,
    val imagem: String,
    val resposta: String
)

