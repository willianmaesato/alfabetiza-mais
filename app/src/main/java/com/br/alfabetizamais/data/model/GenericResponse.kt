package com.br.alfabetizamais.data.model

import kotlinx.serialization.Serializable

@Serializable
data class GenericResponse(
    val success: Boolean,
    val message: String,
    val user: List<UserResponse>
)