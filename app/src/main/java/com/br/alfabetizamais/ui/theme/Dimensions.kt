package com.br.alfabetizamais.ui.theme

import androidx.compose.ui.unit.dp

val paddingSmall = 8.dp
val paddingMedium = 16.dp
val paddingHigh = 24.dp