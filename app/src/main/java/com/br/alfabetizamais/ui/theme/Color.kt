package com.br.alfabetizamais.ui.theme

import androidx.compose.ui.graphics.Color

// Blue
val Blue100 = Color(0xFFBBDEFB)
val Blue200 = Color(0xFF90CAF9)
val Blue300 = Color(0xFF64B5F6)
val Blue400 = Color(0xFF42A5F5)
val Blue500 = Color(0xFF2196F3)
val Blue700 = Color(0xFF1976D2)

// Red
val Red100 = Color(0xFFFFCDD2)
val Red200 = Color(0xFFEF9A9A)
val Red300 = Color(0xFFE57373)
val HighRed = Color(0xFFE53935)
val MediumRed = Color(0xFFEF5350)
val LightRed = Color(0xFFEF9A9A)

// Grey
val Grey500 = Color(0xFF6D6D6D)
val Grey700 = Color(0xFF404040)

val LightGray = Color(0xFFf5f5f5)
val LightPurple = Color(0xFFe4e9ff)
val MediumPurple = Color(0xFF768fdb)
val HighPurple = Color(0xFF303F9F)

// Green
val HighGreen = Color(0xFF43A047)
val MediumGreen = Color(0xFF66BB6A)
val LightGreen = Color(0xFFA5D6A7)

// Blue
val HighBlue = Color(0xFF1E88E5)
val MediumBlue = Color(0xFF42A5F5)
val LightBlue = Color(0xFF90CAF9)

// Amber
val HighAmber = Color(0xFFFB8C00)
val MediumAmber = Color(0xFFFFA726)
val LightAmber = Color(0xFFFFCC80)